-- phpMyAdmin SQL Dump
-- version 4.0.10.20
-- https://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 13 2017 г., 19:31
-- Версия сервера: 5.5.53
-- Версия PHP: 5.4.45

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `bwt_test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `#_kb_category`
--
-- Создание: Окт 02 2017 г., 19:40
--

CREATE TABLE IF NOT EXISTS `#_kb_category` (
  `id` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(9) DEFAULT '0',
  `category` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;

--
-- Дамп данных таблицы `#_kb_category`
--

INSERT INTO `#_kb_category` (`id`, `parent_id`, `category`) VALUES
(1, 0, '2017 год'),
(2, 1, 'Рождественская елка 2017'),
(3, 1, 'Зевс 2017'),
(4, 1, 'Ника 2017'),
(5, 1, 'Звітний концерт 2017'),
(6, 1, 'Тріумф 2017'),
(7, 1, 'Весняна квіточка 2017'),
(8, 1, 'Летняя феерия 2017'),
(9, 1, 'Кубок Березняков 2017'),
(10, 1, 'Аккорды Хортитцы 2017'),
(11, 1, 'Империя юных талантов 2017'),
(12, 1, 'Kinder-Mix 2017'),
(13, 1, 'Maksy Cup 2017'),
(14, 1, 'New Dnipro Stars 2017'),
(15, 1, 'Юлена 2017'),
(16, 1, 'Юбилейный отчетный концерт 2017'),
(17, 1, 'Золотая фея 2017'),
(18, 1, 'Olympic dream 2017'),
(19, 1, 'Перлина Полісся 2017'),
(20, 1, 'Летняя феерия 2017'),
(21, 1, 'Nika Cup 2017'),
(22, 1, 'Золота мрія 2017'),
(23, 1, 'Запорізька Січ 2017'),
(24, 1, 'SummerStar 2017'),
(25, 1, 'Перлина Хортиці 2017'),
(26, 1, 'Кукольный домик'),
(27, 1, 'Azov Cup 2017'),
(28, 1, 'Перлина Южного 2017'),
(29, 1, 'Звездочка Таврии 2017'),
(30, 1, 'Кубок Соломянки 2017'),
(31, 0, '2016 год'),
(32, 31, 'Рождественская елка 2016'),
(33, 0, '2015 год'),
(34, 33, 'Advance - 2015');

-- --------------------------------------------------------

--
-- Структура таблицы `#_kb_video`
--
-- Создание: Окт 03 2017 г., 11:53
--

CREATE TABLE IF NOT EXISTS `#_kb_video` (
  `id` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `id_category` int(9) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `https_video` varchar(50) DEFAULT NULL,
  `content` varchar(255) DEFAULT 'pusto',
  `keywords` varchar(255) DEFAULT 'pusto',
  `description` varchar(255) DEFAULT 'pusto',
  `hit` int(1) DEFAULT '0',
  `new` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=37 ;

--
-- Дамп данных таблицы `#_kb_video`
--

INSERT INTO `#_kb_video` (`id`, `id_category`, `name`, `https_video`, `content`, `keywords`, `description`, `hit`, `new`) VALUES
(1, 29, 'Показательное выступление Евгении Гомон', 'https://www.youtube.com/embed/h938vWNfhH0', 'pusto', 'pusto', 'pusto', 0, 0),
(2, 28, 'Варда Кристина 2013', 'https://www.youtube.com/embed/n0mvageSc-g" ', 'pusto', 'pusto', 'pusto', 0, 0),
(3, 27, 'Показательное выступление Детство', 'https://www.youtube.com/embed/tv7BJVycjtc', 'pusto', 'pusto', 'pusto', 0, 0),
(4, 26, 'Обзор кукольного домика', 'https://www.youtube.com/embed/Jtl3j6HF124', 'pusto', 'pusto', 'pusto', 0, 0),
(5, 25, 'Терещенко Ева 2013', 'https://www.youtube.com/embed/w3vIw5f_XXc', 'pusto', 'pusto', 'pusto', 0, 0),
(6, 24, 'Рубашка Ульяна 2011', 'https://www.youtube.com/embed/yV8ggk_4WCs', 'pusto', 'pusto', 'pusto', 0, 0),
(7, 23, 'Заволокина Ева 2013', 'https://www.youtube.com/embed/5PSQ9uqmXvc', 'pusto', 'pusto', 'pusto', 0, 0),
(8, 22, 'Шкребти Вика 2012', 'https://www.youtube.com/embed/ehAtQCHodGE', 'pusto', 'pusto', 'pusto', 0, 0),
(9, 21, 'заставка Nika Cup', 'https://www.youtube.com/embed/7DGZWhxiX54', 'pusto', 'pusto', 'pusto', 0, 0),
(10, 20, 'анонс Летняя феерия 2017', 'https://www.youtube.com/embed/t2m1NPEVDuE', 'pusto', 'pusto', 'pusto', 0, 0),
(11, 19, 'заставка Перлина Полісся', 'https://www.youtube.com/embed/7GDYOuSWh4s', 'pusto', 'pusto', 'pusto', 0, 0),
(12, 18, 'Калимулина Дарья 2012', 'https://www.youtube.com/embed/kbNVo6yRAp0', 'pusto', 'pusto', 'pusto', 0, 0),
(13, 17, 'Заволокина Ева 2013', 'https://www.youtube.com/embed/NnlQibxpbTQ', 'pusto', 'pusto', 'pusto', 0, 0),
(14, 16, 'Оркестр украинской музыки Танец феи Драже', 'https://www.youtube.com/embed/X7JjWuYa4NQ', 'pusto', 'pusto', 'pusto', 0, 0),
(15, 15, 'Репренцева Ева 2012', 'https://www.youtube.com/embed/m_aLJNQrbrc', 'pusto', 'pusto', 'pusto', 0, 0),
(16, 14, 'Clip New Dnipro Stars 2017', 'https://www.youtube.com/embed/PFqCQ2hncKU', 'pusto', 'pusto', 'pusto', 0, 0),
(17, 13, 'Заволокина Ева 2013', 'https://www.youtube.com/embed/gXU0MqnI9_o', 'pusto', 'pusto', 'pusto', 0, 0),
(18, 12, 'заставка Kinder Mix', 'https://www.youtube.com/embed/e3xuUIZM2aE', 'pusto', 'pusto', 'pusto', 0, 0),
(19, 11, 'Награждение гимнасток 3 день соревнаний', 'https://www.youtube.com/embed/4IUDM0QR6vw', 'pusto', 'pusto', 'pusto', 0, 0),
(20, 10, 'Діордієв Михайло', 'https://www.youtube.com/embed/F0brEULouaY', 'pusto', 'pusto', 'pusto', 0, 0),
(21, 9, 'Варда Кристина 2013', 'https://www.youtube.com/embed/yccB-RoR4Ks', 'pusto', 'pusto', 'pusto', 0, 0),
(22, 8, 'Город говорит Микэ', 'https://www.youtube.com/embed/AI1BiZ9wYZw', 'pusto', 'pusto', 'pusto', 0, 0),
(23, 7, 'заставка Весняна Квіточка', 'https://www.youtube.com/embed/5rWWMIVLSKo', 'pusto', 'pusto', 'pusto', 0, 0),
(24, 6, 'заставка Триумф', 'https://www.youtube.com/embed/9sRlpLlm-zE', 'pusto', 'pusto', 'pusto', 0, 0),
(25, 5, 'Оркестр народних інструментів. Чайковський. «Танець феї Драже»', 'https://www.youtube.com/embed/8l-Mvz3vrtw', 'pusto', 'pusto', 'pusto', 0, 0),
(26, 4, 'Дудкина Маша 2012', 'https://www.youtube.com/embed/g8gWLrn6EJ4', 'pusto', 'pusto', 'pusto', 0, 0),
(27, 3, 'Каневская Варвара 2012', 'https://www.youtube.com/embed/oydjZnEO4w4', 'pusto', 'pusto', 'pusto', 0, 0),
(28, 2, 'Балаева Василиса  2011', 'https://www.youtube.com/embed/Rfqdn_Ne7x4', 'pusto', 'pusto', 'pusto', 0, 0),
(29, 32, 'заставка', 'https://www.youtube.com/embed/v8kQE30RWMU', 'pusto', 'pusto', 'pusto', 0, 0),
(30, 34, 'заставка', 'https://www.youtube.com/embed/LoKIV5tRtOY', 'pusto', 'pusto', 'pusto', 0, 0),
(31, 30, 'заставка Кубок Соломянки', 'https://www.youtube.com/embed/gX7wuO_v_rc', 'pusto', 'pusto', 'pusto', 0, 0),
(32, 29, 'Показательное выступление Инны Ищенко и Алины Максименко', 'https://www.youtube.com/embed/Fpg7pqK-N3I', 'pusto', 'pusto', 'pusto', 0, 0),
(33, 29, 'Прямая трансляция пользователя karinbutan «ЗВЕЗДОЧКА ТАВРИИ-2017»', 'https://www.youtube.com/embed/M0KcLgnz-xU', 'pusto', 'pusto', 'pusto', 0, 0),
(34, 3, 'Божук Вера 2012', 'https://www.youtube.com/embed/xI4lldpGhag', 'pusto', 'pusto', 'pusto', 0, 0),
(35, 3, 'Кореневская Лиза 2011', 'https://www.youtube.com/embed/GijCAAQh3U8', 'pusto', 'pusto', 'pusto', 0, 0),
(36, 3, 'Филипп Аня 2011', 'https://www.youtube.com/embed/IULZmd1fxGc', 'pusto', 'pusto', 'pusto', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `feedback`
--
-- Создание: Окт 02 2017 г., 19:58
-- Последнее обновление: Окт 13 2017 г., 09:03
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `email` varchar(32) NOT NULL,
  `text` text NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `feedback`
--

INSERT INTO `feedback` (`id`, `name`, `email`, `text`, `data`) VALUES
(1, 'igor', 'kb_sk@mail.ru', 'Привет!\r\nКак дела :)', '2017-01-15 14:43:52'),
(7, 'Петя', 'kb_sk@mail.ru.ua', 'Привет! Привет!', '2017-01-16 05:57:23'),
(8, 'Саша', 'cv@mail.ru', '&lt;h1&gt;Проверка html в сообщении&lt;h1&gt;', '2017-01-16 06:15:13'),
(9, 'Рома', 'sd@mail.ru', 'Все  ок!', '2017-01-16 06:18:02'),
(10, 'Игорь', 'kb_sk@mail.ru.zp', 'Вывод сообщений отсортированный по возрастанию.', '2017-01-16 06:43:25'),
(12, 'igor', 'kb@ukr.net', 'dasdadasdasddassdasdsadsda\r\nsdfsdfsfsdf\r\nfsdfsfdsfsdf', '2017-10-13 09:03:17');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--
-- Создание: Окт 11 2017 г., 21:12
-- Последнее обновление: Окт 13 2017 г., 15:02
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `name` varchar(20) NOT NULL COMMENT 'Имя',
  `surname` varchar(20) NOT NULL COMMENT 'Фамилия',
  `day` varchar(2) NOT NULL COMMENT 'День',
  `month` varchar(10) NOT NULL COMMENT 'Месяц',
  `year` varchar(4) NOT NULL COMMENT 'Год',
  `email` varchar(32) NOT NULL COMMENT 'Адрес',
  `password` varchar(255) NOT NULL COMMENT 'Пароль',
  `sex` varchar(10) NOT NULL COMMENT 'Пол',
  `user_hash` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `username`, `name`, `surname`, `day`, `month`, `year`, `email`, `password`, `sex`, `user_hash`) VALUES
(4, 'admin', 'igor', 'baraban', '13', '06', '1978', 'kb_sk@mail.ru', '$2y$13$GMzQybwvjqSsNKrX6IjnLOm7HYSa9C9oDLXcN/B9uV.3Kno01oVvS', 'Мужской', 'EjjEmTH9JSPlcWSJUNI2jyxeh3vzrWK-'),
(6, 'demo', 'd', 'm', '1', '1', '1999', 'a@ukr.net', '$2y$13$exc2hDm/LitJN4Y06buZ1.rN/9aFjtjuvxS/pLYN63r4XyMRpQ3y2', 'Мужской', '2dsgQOTyT7on-VenrFgQmRhzIbyT1imC');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
