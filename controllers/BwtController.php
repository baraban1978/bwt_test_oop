<?php

/**

 * Created by PhpStorm.

 * User: Администратор

 * Date: 14.05.17

 * Time: 23:37

 */



namespace app\controllers;

use app\models\Video;
use app\models\Category;
use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Gismeteo;
use app\models\SignupForm;
use app\models\User;
use app\models\Feedbacks;
use yii\data\ActiveDataProvider;

/**
 * Class BwtController
 * @package app\controllers
 */

class BwtController extends Controller{

    public $layout = 'test';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['login', 'logout', 'page-two', 'page-four'],

                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['logout', 'page-two', 'page-four'],
                        'roles' => ['@'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],

        ];

    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
                'maxLength' => 8, // максимальная длинна капчи
                'minLength' => 6, // минимальная длинна капчи
                'backColor'=> 0xFF0000, // фон красного цвета
                'foreColor'=> 0xFFFFFF, // шрифт белого цвета
            ],
        ];
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    public function actionIndex(){



        return $this->render('index');

    }


    public function actionPageOne(){
        $day = ['1','2','3','4','5','6','7','8','9','10',
            '11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'];
        $month=['январь','февраль','март','апрель','май','июнь ','июль','август','сентябрь','октябрь','ноябрь','декабрь'];
        $year=['1971','1972','1973','1974','1975','1976','1977','1978','1979',
            '1980','1981','1982','1983','1984','1985','1986','1987','1988','1989',
            '1990','1991','1992','1993','1994','1995','1996','1997','1998','1999',
            '2000','2001','2002','2003','2004','2005','2006','2007','2008','2009'];
        $sex=['Женский', 'Мужской'];
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new SignupForm();
        if($model->load(\Yii::$app->request->post()) && $model->validate()){
            $user = new User();
            $user->username = $model->username;
            $user->password = \Yii::$app->security->generatePasswordHash($model->password);
            $user->name = $model->name;
            $user->surname = $model->surname;
            $user->day = $day[$model->day];
            $user->month = $month[$model->month];
            $user->year = $year[$model->year];
            $user->email = $model->email;
            $user->sex = $sex[$model->sex];

            if($user->save()){
                Yii::$app->session->setFlash('success', 'Данные приняты');
                return $this->refresh();
            }else{
                Yii::$app->session->setFlash('error', 'Ошибка');

            }
}

        return $this->render('pageone', compact('model'));

    }

    public function actionPageTwo(){

        // get cache
        $pogoda = Yii::$app->cache->get('pogoda');
        $text = Yii::$app->cache->get('text');
        if($pogoda) return $this->render('pagetwo', [
            'pogoda' => $pogoda,
            'text' => $text,]);

        $pogoda = new Gismeteo();
        $text = $pogoda->text();
        $pogoda = $pogoda->getArrayHtml();
        // set cache
        Yii::$app->cache->set('pogoda', $pogoda, 3600);
        Yii::$app->cache->set('text', $text, 3600);

        return $this->render('pagetwo', [
            'pogoda' => $pogoda,
            'text' => $text,

        ]);

    }

    public function actionPageThree(){

        $model = new ContactForm();


        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()){
                Yii::$app->session->setFlash('contactFormSubmitted');
                $feedback = new Feedbacks();
                $feedback->name = $model->name;
                $feedback->email = $model->email;
                $feedback->text = $model->text;

                if($feedback->save()){
                    Yii::$app->session->setFlash('success', 'Данные приняты');
                    return $this->refresh();
                }else{
                    Yii::$app->session->setFlash('error', 'Ошибка');

                }

            }else{
                Yii::$app->session->setFlash('error', 'Ошибка');
            }

        }

        return $this->render('pagethree', compact('model'));

    }

    public function actionPageFour(){
        $Provider = new ActiveDataProvider([
            'query' =>Feedbacks::find(),
            'pagination' => [
                'pageSize' => 5,
            ],
            'sort' => [
                'defaultOrder' => [
                    'data' => SORT_DESC,
                    //'data' => SORT_ASC,
                ]
            ],
        ]);


        return $this->render('pagefour', compact('Provider'));

    }

    public function actionPageFive(){

        return $this->render('pagefive');

    }

    public function actionView($id){
        $id=Yii::$app->request->get('id');
        $video=Video::find()->where(['id_category'=>$id])->all();
        $cat=Category::find()->where(['id'=>$id])->all();
        $cat=$cat[0]->category;

        return $this->render('view', compact('video', 'cat'));

    }





} 