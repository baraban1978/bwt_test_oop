<li>
    <a href="<?= \yii\helpers\Url::to(['bwt/view', 'id' => $category['id']]) ?>">
        <?= $category['category']?>
        <?php if( isset($category['childs']) ): ?>
<!--            <span class="badge pull-right"><i class="fa fa-plus"></i></span>-->
            <span class="badge pull-right glyphicon glyphicon-plus"></span>

        <?php endif;?>
    </a>
    <?php if( isset($category['childs']) ): ?>
        <ul>
            <?= $this->getMenuHtml($category['childs'])?>
        </ul>
    <?php endif;?>
</li>