<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 03.10.17
 * Time: 23:25
 */
namespace app\components;
use yii\base\Widget;
use app\models\Category;
use Yii;

class CategoryWidget extends Widget{
    public $tpl;
    public $data;
    public $tree;
    public $menuHtml;

    public function init(){
        parent::init();
        if ($this->tpl === null){
            $this->tpl = 'menu';
        }
        $this->tpl .= '.php';
    }

    public function run(){
        // get cache
        $menu = Yii::$app->cache->get('menu');
        if($menu) return $menu;

        $this->data=Category::find()->indexBy('id')->asArray()->all();
        $this->tree=$this->getTree();
        $this->menuHtml=$this->getMenuHtml($this->tree);
        // set cache
        Yii::$app->cache->set('menu', $this->menuHtml, 3600);


        return $this->menuHtml;
    }
    protected function getTree(){
        $tree=[];
        foreach ($this->data as $id=>&$node) {
            if(!$node['parent_id'])
                $tree[$id]=&$node;
            else
                $this->data[$node['parent_id']]['childs'][$node['id']]=&$node;

        }
        return $tree;
    }

    protected function getMenuHtml($tree){
        $str = '';
        foreach ($tree as $categorja) {
            $str .= $this->catToTemplate($categorja);
        }
        return $str;
    }

    protected function catToTemplate($category){
        ob_start();
        include __DIR__ . '/category_tpl/' . $this->tpl;
        return ob_get_clean();
    }



} 