<?php


use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Главная';
?>
<h3>Тестовое задание</h3><br/>

<h5>- создать аккаунт на bitbucket (если его нет) <br/>
- создать приватный проект  bwt_test <br/>
- все изменения в коде вести через git <br/>
- создать 4 страницы с одной точкой входа <br/>
- при создании страниц использовать Model-view-controller <br/>
    <?= Html::a("Страница1", Url::to(["/page-one"]))?> - форма регистрации. Поля: имя, фамилия, email, пол, день рождения password(пол и день рождения - необязательные поля. валидация должна быть и на стороне клиента, и на стороне сервера) <br/>
    <?= Html::a("Страница2", Url::to(["/page-two"]))?> - отображать погоду на сегодня в городе Запорожье (только зарегистрированные пользователи имеют доступ). Данные парсить по этойссылке http://www.gismeteo.ua/city/daily/5093/<br/>
    <?= Html::a("Страница3", Url::to(["/page-three"]))?> - форма обратной связи. поля: имя, email, сообщение (все поля обязательные, валидация должна быть и на стороне клиента, и на сторонесервера). Также добавить капчу. <br/>
    <?= Html::a("Страница4", Url::to(["/page-four"]))?> - отображать список фидбеков, оставленных на странице 3.(только зарегистрированные пользователи имеют доступ) <br/>
- для верстки использовать framework bootstrap 3<br/>
- для парсинга погоды можно использовать пакет guzzle ( https://github.com/guzzle/guzzle ) <br/>
- создать ER-диаграму структуры созданной базы данных. Так же положить ее в git <br/>
- добавить пользователя bwt_account к проекту <br/>
- прислать нам ссылку на bitbucket проект<br/></h5>

