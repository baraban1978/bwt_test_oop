<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = [
    'template' => "<li>{link}</li>\n", //  шаблон для этой ссылки
    'label' => 'Главная', // название ссылки
    'url' => ['/'] // сама ссылка
];
$this->params['breadcrumbs'][] = $this->title;


?>


<div class="bwt-page">


    <?php if( Yii::$app->session->hasFlash('success') ): ?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo Yii::$app->session->getFlash('success'); ?>
        </div>
    <?php endif;?>

    <?php if( Yii::$app->session->hasFlash('error') ): ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo Yii::$app->session->getFlash('error'); ?>
        </div>
    <?php endif;?>


<?php $form = ActiveForm::begin(
    [
        'id' => 'signup-form',
        'layout'=>'horizontal',
        'options' => ['class' => 'signup-form form-register1'],
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-4',
                'offset' => 'col-sm-offset-4',
                'wrapper' => 'col-sm-8',
                'error' => '',
                'hint' => '',
    ]
    ]]
) ?>
    <?= Html::style('div.required label.control-label:after {
    content: " *";
    color: red;
}') ?>
<?= $form->field($model, 'username') ?>
<?= $form->field($model, 'name') ?>
<?= $form->field($model, 'surname') ?>
<?= $form->field($model, 'day')->dropdownList(['1','2','3','4','5','6','7','8','9','10',
    '11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31']); ?>
<?= $form->field($model, 'month')->dropdownList(['январь','февраль','март','апрель','май','июнь ','июль','август','сентябрь','октябрь','ноябрь','декабрь']); ?>
<?= $form->field($model, 'year')->dropdownList(['1971','1972','1973','1974','1975','1976','1977','1978','1979',
    '1980','1981','1982','1983','1984','1985','1986','1987','1988','1989',
    '1990','1991','1992','1993','1994','1995','1996','1997','1998','1999',
    '2000','2001','2002','2003','2004','2005','2006','2007','2008','2009']); ?>
<?= $form->field($model, 'email') ?>
<?= $form->field($model, 'sex')->dropdownList(['Женский', 'Мужской']) ?>
<?= $form->field($model, 'password')->passwordInput() ?>

    <div class="col-sm-4">
    </div>
        <div class="col-sm-8">

            <?= Html::submitButton('Регистрация', ['class' => 'btn btn-success']) ?>

        </div>



<?php ActiveForm::end() ?>

</div>