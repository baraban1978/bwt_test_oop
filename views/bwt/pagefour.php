<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;


$this->title = 'Отзывы';
$this->params['breadcrumbs'][] = [
    'template' => "<li>{link}</li>\n", //  шаблон для этой ссылки
    'label' => 'Главная', // название ссылки
    'url' => ['/'] // сама ссылка
];
$this->params['breadcrumbs'][] = $this->title;


?>

<?= GridView::widget([
'dataProvider' => $Provider,
'columns' => [
//['class' => 'yii\grid\SerialColumn'],
'data',
'name',
'email',
    'text',
//['class' => 'yii\grid\ActionColumn'],
],
]); ?>