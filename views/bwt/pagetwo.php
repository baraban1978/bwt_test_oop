<?php
use yii\helpers\Html;

$this->registerCssFile('@web/css/origin_18fe88d4.css', ['depends' => ['app\assets\AppAsset']]);
$this->title = 'Погода';
$this->params['breadcrumbs'][] = [
    'template' => "<li>{link}</li>\n", //  шаблон для этой ссылки
    'label' => 'Главная', // название ссылки
    'url' => ['/'] // сама ссылка
];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="bwt-page">
    <h3>Погода на сегодя в Запорожье. Источник http://www.gismeteo.ua/city/daily/5093/</h3>
</div>


<div class="row">
    <div class="col-xs-12 col-sm-6">
         <?=$pogoda['block'];?>
    </div>
    <div class="col-xs-12 col-sm-6">
        <p class="bg-primary text-center"><?=$text;?></p>
    </div>
</div>


<!--    <p>-->
<!--        Это страница Pagetwo. Вы можете изменить следующий файл, чтобы настроить еe содержимое:-->
<!--    </p>-->
<!---->
<!--    <code>--><?//= __FILE__ ?><!--</code>-->
