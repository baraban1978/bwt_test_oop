<?php
use yii\helpers\Html;

$this->title = 'Видео архив';
$this->params['breadcrumbs'][] = [
    'template' => "<li>{link}</li>\n", //  шаблон для этой ссылки
    'label' => 'Главная', // название ссылки
    'url' => ['/'] // сама ссылка
];
$this->params['breadcrumbs'][] = $this->title;


?>


<div class="bwt-page">
    <h1><?= Html::encode($this->title) ?></h1>

</div>

<div class="container ">
    <div class="row">
        <div class="col-sm-4">
            <ul class="catalog category-products">

                <?= \app\components\CategoryWidget::widget(['tpl'=>'menu']) ?>
            </ul>
        </div>
        <div class="col-sm-8">

        </div>
    </div>
</div>
