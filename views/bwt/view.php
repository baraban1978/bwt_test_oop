<?php
use yii\helpers\Html;

$this->title = 'Видео архив';
$this->params['breadcrumbs'][] = [
'template' => "<li>{link}</li>\n", //  шаблон для этой ссылки
'label' => 'Главная', // название ссылки
'url' => ['/'] // сама ссылка
];
$this->params['breadcrumbs'][] = $this->title;


?>


<div class="bwt-page">
    <h1><?= Html::encode($this->title) ?></h1>

</div>

<div class="container ">
    <div class="row">
        <div class="col-sm-4">
            <ul class="catalog category-products">

                <?= \app\components\CategoryWidget::widget(['tpl'=>'menu']) ?>
            </ul>
        </div>
        <div class="col-sm-8">
            <div class="row">
                <?php if (!empty($video)):?>
                    <h2> <?= $cat ?></h2>
                    <?php foreach ($video as $block) : ?>
                       <div>
                           <h3><?= $block->name ?></h3>
                       <iframe width="560" height="315" src='<?= $block->https_video ?>' frameborder="1" allowfullscreen></iframe>

                       </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <h2>Здесь пока видео нет.</h2>

                <?php endif; ?>

            </div>
        </div>
    </div>
</div>
