<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 13.10.17
 * Time: 11:23
 */

namespace app\models;

use yii\db\ActiveRecord;

class Feedbacks extends ActiveRecord{

    public static function tableName(){
        return 'feedback';
    }

    public function getUser(){
        return $this->hasOne(User::className(),['email' => 'email']);
    }

} 