<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 14.09.17
 * Time: 8:25
 */

namespace app\models;


class Gismeteo {

    //Киев - "https://www.gismeteo.ua/weather-kyiv-4944/"
    //Днепр - "https://www.gismeteo.ua/weather-dnipro-5077/"
    //Запорожье
    public $url = "https://www.gismeteo.ua/weather-zaporizhia-5093/";


    public function getArrayHtml(){
        ///curl_init() создает новый сеанс CURL и возвращает дескриптор, который используется с функциями curl_setopt(),
        $curl = curl_init($this->url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $str = curl_exec($curl);
        curl_close($curl);
        $html= str_get_html($str);

        $array = [
            "block" => $html->find('div[id=weather]', 0)->outertext,
            "text" => $html->find('div[id=weather] div div', 0)->plaintext,
            "temp" => $html->find('div[id=weather].temp dd' , 0)->plaintext,
            "wind1" => $html->find('div[id=weather].wind dd' , 0)->plaintext,
            "wind2" => $html->find('div[id=weather].wind dt' , 0)->plaintext,
            "barp" => $html->find('div[id=weather].barp dd' , 0)->plaintext,
            "hum" => $html->find('div[id=weather].hum' , 0)->plaintext,
            "water" => $html->find('div[id=weather].water dd' , 0)->plaintext,
            "cloudness" => $html->find('dl[id=weather].cloudness dd' , 0)->plaintext,
            "time" => $html->find('div[id=weather].f_link' , 0)->plaintext,
        ];

        //освобождаем ресурсы
        $html->clear();
        unset($html);

        return $array;
    }


    public function getUrl()
    {
        return $this->url;
    }

    public function text()
    {
        $pogoda = $this->getArrayHtml();
        $text = '<br> <br> ' . $pogoda['time']
            . ' <br> Погода в Запорожье - ' . $pogoda['cloudness']
            . '. <br> Температура воздуха ' . $pogoda['temp']
            . '. <br> Ветер ' . $pogoda['wind1'] . '. Направление ' . $pogoda['wind2']
            . '. <br> Давление ' . $pogoda['barp']
            . '. <br>' .  $pogoda['hum'] . ' воздуха'
            . '. <br>' . $pogoda['water'] . ' в Днепре.'. ' <br>'. ' <br> <br>';
        return $text;
    }

} 