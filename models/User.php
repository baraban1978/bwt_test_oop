<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;


class User extends ActiveRecord implements IdentityInterface
{
    public static function tableName()
    {
        return 'users';
    }


    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
//        return static::findOne(['access_token' => $token]);
    }
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    public function getId()
    {
        return $this->id;
    }


    public function getAuthKey()
    {
        return $this->user_hash;
    }


    public function validateAuthKey($authKey)
    {
        return $this->user_hash === $authKey;
    }

    public function validatePassword($password)
    {
//        return $this->password === $password;
        return \Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->user_hash = \Yii::$app->getSecurity()->generateRandomString();
            }
            return true;
        }
        return false;
    }

    public function generateAuthKey()
    {
        $this->user_hash = \Yii::$app->getSecurity()->generateRandomString();
    }

    public function getFeedback(){
        return $this->hasMany(Feedback::className(),['email' => 'email']);
    }


}



