<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 09.10.17
 * Time: 21:02
 */

namespace app\models;


use yii\base\Model;

class SignupForm extends Model{
    public $username;
    public $name;
    public $surname;
    public $day;
    public $month;
    public $year;
    public $email;
    public $password;
    public $sex;

    public function rules() {
       return [
           [['username',
               'name',
               'surname',
               'email',
               'password'], 'required', 'message' => 'Заполните поле'],
           [['username',
               'name',
               'surname',
               'email',
               'password',
               'day',
               'month',
               'year',
               'email',
               'sex'], 'trim'],
           ['email', 'email'],
           ['username', 'unique', 'targetClass' => User::className(),  'message' => 'Этот логин уже занят'],
           ['email', 'unique', 'targetClass' => User::className(),  'message' => 'Пользователь с таким email уже заригестрирован'],

       ];
    }

    public function attributeLabels() {
        return [
            'username' => 'Логин',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'day' => 'День',
            'month' => 'Месяц',
            'year' => 'Год',
            'email' => 'email',
            'password' => 'Пароль',
            'sex' => 'Пол',
        ];
    }
}