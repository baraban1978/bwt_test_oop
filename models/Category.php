<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 03.10.17
 * Time: 22:22
 */

namespace app\models;
use yii\db\ActiveRecord;


class Category extends ActiveRecord {

    public static function tableName() {
        return '#_kb_category';
    }

    public function getVideos(){
        return $this->hasMany(Video::className(), ['id_category'=>'id']);
    }
} 