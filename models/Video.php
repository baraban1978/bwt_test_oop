<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 03.10.17
 * Time: 22:35
 */

namespace app\models;
use yii\db\ActiveRecord;


class Video extends ActiveRecord {

    public static function tableName() {
        return '#_kb_video';
    }

    public function getCategory(){
        return $this->hasOne(Category::className(), ['id'=>'id_category']);
    }
} 